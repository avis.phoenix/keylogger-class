#ifndef WINDOWSKEYBOARDHOOK_H
#define WINDOWSKEYBOARDHOOK_H

#include <Windows.h>
#include <string>

using std::string;

typedef void(*WKHCallBackFunc)(DWORD,BOOL,BOOL);

class WindowsKeyboardHook
{
public:
    WindowsKeyboardHook();
    ~WindowsKeyboardHook();
    bool initHook();
    static string code2Str(DWORD code, BOOL caps, BOOL shift);
    static bool getKeyState(int virtKey){return GetKeyState(virtKey) > 0;}
    // function when button press
    static WKHCallBackFunc callbackFunc;
private:
    // KeyBoard hook handle in global scope
    HHOOK KeyboardHook;
    // Shift Key
    static bool shift;
    //Hook
    static LRESULT HookProcedure(int nCode, WPARAM wParam, LPARAM lParam);
};

#endif // WINDOWSKEYBOARDHOOK_H
